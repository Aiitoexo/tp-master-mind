let attMark = "f" + checkCombinaison.turn;
let attLight = "g" + checkCombinaison.turn;

let buttons = document.querySelectorAll(".secret .button-selector");
let lights = document.querySelectorAll("#" + `${attLight}` + " .combination");
let markerLights = document.querySelectorAll("#" + `${attMark}` + " .mark");
let validButton = document.querySelector(".validation");
let msgLoose = document.querySelector(".msg-loose");
let msgWin = document.querySelector(".msg-win");
let reset = document.querySelectorAll(".reset");
let resultGame = document.querySelectorAll(".result-game-win .combination")
let resultGameLoose = document.querySelectorAll(".result-game-loose .combination")

for (let btn of reset) {
    btn.addEventListener("click", function () {
        verificationEndGame.reset();
    });
}

function selectColor() {
    if (this.clickCount === 3) this.clickCount = 0;
    else this.clickCount++;
    console.log(this.clickCount);
    this.connexion().style.background = "none";
    this.connexion().style.backgroundColor = randomCombinaisonColor.allColor[this.clickCount];
}

function combinaison() {
    checkCombinaison.validateCombinaison();
}

for (let i in buttons) {
    if (isFinite(i)) {
        buttons[i].clickCount = -1;
        buttons[i].connexion = () => lights[i];
        buttons[i].addEventListener("click", selectColor);
    }
}

validButton.addEventListener("click", combinaison);

randomCombinaisonColor.generateCombinaison();