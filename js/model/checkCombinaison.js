const checkCombinaison = {

    turn: 1,

    setColor: [],

    matchColor: [],

    validateCombinaison() {

        this.setColor = []

        for (let i = 0; i < lights.length; i++) {

            if (lights[i].style.backgroundColor === "") {

                this.setColor= []
                return

            } else {

                this.setColor.push(lights[i].style.backgroundColor)
                this.checkColor();

            }
        }

        this.printMatch()

        this.turn++

        verificationEndGame.verifWinner()

        if (this.turn > 10) {
            verificationEndGame.loose()
        }
        attMark = "f" + this.turn;
        attLight = "g" + this.turn;
        lights = document.querySelectorAll("#" + `${attLight}` + " .combination");
        markerLights = document.querySelectorAll("#" + `${attMark}` + " .mark");

    },

    checkColor() {

            this.matchColor = [];

            for (let i = 0; i < randomCombinaisonColor.difficulty; i++) {

                if (this.setColor[i] === randomCombinaisonColor.getCombinaison()[i]) {
                    this.matchColor.push("red")
                } else if (randomCombinaisonColor.getCombinaison().includes(this.setColor[i])) {
                    this.matchColor.push("white")
                } else {
                    this.matchColor.push("black")
                }

            }

    },

    printMatch() {

        for (let i = 0; i < this.matchColor.length; i++) {
            markerLights[i].style.background = "none"
            markerLights[i].style.background = this.matchColor[i]
        }
        this.matchColor = []
    },

}