const randomCombinaisonColor = {

    difficulty: 4,

    allColor: ["blue", "red", "yellow", "green"],
    randomCombinaison: [],

    generateCombinaison() {

        for (let i = 0; i < this.difficulty; i++) {
            this.randomCombinaison[i] = this.allColor[Math.floor(Math.random() * this.allColor.length)];
        }

    },

    getCombinaison() {

        return this.randomCombinaison;

    }

}