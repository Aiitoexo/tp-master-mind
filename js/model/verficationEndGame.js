const verificationEndGame = {

    verifWinner() {

        for (let i = 0; i < checkCombinaison.setColor.length; i++) {

            if (checkCombinaison.setColor[i] !== randomCombinaisonColor.getCombinaison()[i]) {
                return
            }

        }

            this.endGame()
            this.winner()

    },

    resultGame() {

        for (let i = 0; i < randomCombinaisonColor.getCombinaison().length; i++) {

            resultGame[i].style.background = randomCombinaisonColor.getCombinaison()[i]
            resultGameLoose[i].style.background = randomCombinaisonColor.getCombinaison()[i]

        }

    },

    winner() {
        this.endGame();
        msgWin.classList.add("visible");
        this.resultGame()
    },

    loose() {
        this.endGame();
        msgLoose.classList.add("visible");
        this.resultGame()
    },

    endGame() {
        for (let btn of buttons) {
            btn.removeEventListener("click", selectColor);
        }

        validButton.removeEventListener("click", combinaison);
    },

    reset() {
        document.location.reload();
    }

}

